= Quarkus - the rising star
Frederik Hahne
2022-07-20
:jbake-type: post
:jbake-tags: talk, meetup
:jbake-status: published
:icons: font

[IMPORTANT,role="summary"]
.Auf einen Blick
====
icon:calendar[] *Am 20.07.2022 um 18 Uhr*

icon:map-marker[] https://public.senfcall.de/de/jug-paderborn[Online, window=_blank]

icon:comments-o[] https://www.redhat.com/en/authors/jochen-cordes[Jochen Cordes, window=_blank]

====

image:hero_worldtour.png[flyer, 500, 500, link="https://quarkus.io/worldtour/", role="right", window=_blank] 


Am Mittwoch, den *20.07.2022* lädt die Java User Group Paderborn zum Vortrag *Quarkus - the rising star* mit
https://www.redhat.com/en/authors/jochen-cordes[Jochen Cordes, window=_blank] ein.

== Der Vortrag

Quarkus is the rising star for Kube Native Java as it re-imagines the Java stack to 
give you the performance characteristics and developer experience you need to create modern, 
high performing applications. 
Quarkus helps you use your existing skills and code in new ways and greatly 
reduces the technical burden when moving to a Kubernetes-centric environment.

Join us as we explore how Quarkus helps Java developers everywhere to be more productive, 
create modern masterpieces and have a little bit more fun.

== Treffpunkt

Diesmal treffen wir uns ONLINE. Hierzu könnt ihr ab ca. https://public.senfcall.de/de/jug-paderborn[17:45 dem Raum beitreten, window=_blank].
