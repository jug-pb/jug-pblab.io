= 19 Teilnehmer beim ersten Stammtisch
Frederik Hahne
2017-02-27
:jbake-type: post
:jbake-tags: stammtisch, meetup
:jbake-status: published

image:IMG_20170222_191723.jpg[Namesigns, 200, 200, link="IMG_20170222_191723.jpg", float="right"] Am 22.2.2017 fand der erste Stammtisch der JUG Paderborn im Feuerstein statt. Die Resonanz war mehr als hervorragend! 
Neben 16 Anmeldungen via Eventbrite haben sich auch spontan noch 3 weitere Entwickler und Softwerker
dazu entschlossen teilzunehmen, sodass am Ende 19 Teilnehmer an der langen Tafel im Feuerstein Platz nehmen konnten.

Nach einer kurzen Vorstellungsrunde und der Verteilung unserer JUG Aufklebers an alle Teilnehmer wurde ein breites Spektrum von Themen im Lean Coffee Format angeregt diskutiert. 
Es ging von technischen Themen über Organisatorische Themen bis zu Entwicklungsmethodiken. Nach knapp 2.5 Stunden 
löste sich der Stammtisch langsam auf, alle Beteiligten waren sich jedoch einig: Das muss wiederholt werden!

image:IMG_20170222_194323.jpg[Talks-2, 200, 200, link="IMG_20170222_194323.jpg", float="right"]  Der nächste Stammtisch findet daher am 26.04.2017 um 19.30 erneut im Feuerstein Paderborn statt. Die 
https://www.eventbrite.com/e/jug-paderborn-stammtisch-tickets-32386292272:[Anmeldung ist wieder via Eventbrite möglich].

Falls jemand eine Idee oder selber einen Vortrag halten möchte, in den Räumen unserer beiden Sponsoren 
http://www.verlinked.de/:[verlinked] und https://wescale.com/:[wescale] ist das möglich. 
https://gitlab.com/jug-pb/Talks:[Unser Issue Tracker steht offen und jeder kann seine Idee/Wunsch kundtun].

[[img-table-01]]
.Gute Gespräche in lockerer Atmosphäre
image::IMG_20170222_195056.jpg[Talks-1, 200, 200, link="IMG_20170222_195056.jpg"] 

[[img-table-03]]
.Auch bei Essen und Getränken wurde fleißig diskutiert
image::C5TCKLhWIAAK7NA.jpg[Talks-3, 200, 200, link="C5TCKLhWIAAK7NA.jpg"] 