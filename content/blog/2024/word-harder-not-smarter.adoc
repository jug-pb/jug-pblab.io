= Work harder, not smarter
Frederik Hahne
2024-05-09
:jbake-type: post
:jbake-tags: talk, meetup
:jbake-status: published
:icons: font

[IMPORTANT,role="summary"]
.Auf einen Blick
====
icon:calendar[] *Am 22.05.2024 um 18.00 Uhr*

icon:map-marker[] https://enpit.de/[enpit GmbH & Co. KG, window=_blank], https://www.openstreetmap.org/?mlat=51.71677&mlon=8.75216#map=19/51.71677/8.75216[Marienplatz 11 a, 33098 Paderborn, window=_blank]

icon:comments-o[] https://mastodon.social/@meistermeier[Gerrit Meier, window=_blank]

icon:check-square[] https://www.eventbrite.com/e/work-harder-not-smarter-tickets-897911055137[Eventbrite, window=_blank]

icon:gratipay[] https://enpit.de/[enpit GmbH & Co. KG, window=_blank]
====


Am Mittwoch, den *22.05.2024* lädt die Java User Group Paderborn zum Vortrag 
*Work harder, not smarter* mit 
https://mastodon.social/@meistermeier[Gerrit Meier, window=_blank] ein.

== Der Vortrag

Hier ein kleines Bisschen Abstraktion, dort ein bisschen Vererbung und ein paar Wochen später finden wir uns vor dem (virtuellen) Whiteboard wieder, um unsere eigenen Konzepte zu verstehen.
Dabei hatte doch alles so gut begonnen und wir wollten durch Best-Practices die Entwicklungs- und späteren Wartungskosten so gut es geht reduzieren.
Scheinbar haben wir uns (erneut) selber einen Stock zwischen die Speichen gesteckt, indem wir uns auf Frameworks und Tools gestürzt haben, die uns im Endeffekt ermöglichten zu viele Gedanken in unser quasi-wissenschaftliches Domainmodell fließen zu lassen.
Auch über unsere eigenen Anwendungen hinaus suggerieren Tools und Frameworks eine Leichtigkeit, die wir gerne schnell akzeptieren, wobei wir meist nicht auf den ersten Blick erkennen, dass die Kosten dafür eine Verschiebung des Aufwands sind.
In diesem Vortrag, möchte ich auf leicht humorvolle Weise auf die Fallen bei der Softwareentwicklung hinweisen, in die wir heutzutage immer mal wieder gerne laufen.
Dabei geht es in keinem Fall um Fingerpointing, sondern um das Gefühl, dass wir das alles schon einmal erlebt und selber verbrochen haben.


== Der Sprecher

Gerrit Meier ist leidenschaftlicher Softwareentwickler und arbeitet als Software Entwickler bei Neo4j. Neue Technologien, Ideen und Ansätze, auch abseits seines technologischen Schwerpunktes, motivieren ihn immer wieder zum Ausprobieren und Verstehen. Da er dieses als eine der wichtigsten Kompetenzen in der Softwareentwicklungswelt sieht, bemüht er sich sowohl beruflich als auch privat, dieses Wissen und den Spaß am "Einfach-mal-machen" zu vermitteln und zugänglich zu machen. Dies ist auch einer der Gründe, aus denen Gerrit aktiv die JUG Ostfalen unterstützt.

== Treffpunkt

https://enpit.de/[enpit GmbH & Co. KG, window=_blank], Marienplatz 11 a, 33098 Paderborn

NOTE: Zur Planung des Caterings ist eine Anmeldung via https://www.eventbrite.com/e/work-harder-not-smarter-tickets-897911055137[Eventbrite, window=_blank] wünschenswert.